/**
 * Created by shlomim on 19/03/17.
 */
import Vue from 'vue';
import honestbrew from '../components/honestbrew/honestbrew.vue';

new Vue({
    el: '#app',
    components: { honestbrew }
})
