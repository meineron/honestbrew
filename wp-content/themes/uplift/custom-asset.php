<?php

    /*
    *
    *	Swift Page Builder - Animated Headline Shortcode
    *	------------------------------------------------
    *	Swift Framework
    * 	Copyright Swift Ideas 2016 - http://www.swiftideas.com
    *
    */

    class SwiftPageBuilderShortcode_spb_custom_assets extends SwiftPageBuilderShortcode {

        protected function content( $atts, $content = null ) {

            $width = $el_class = $output = $headline_class = $el_position = '';

            extract( shortcode_atts( array(
                'middle_text'       => '',
                'width'       => '1/1',
                'el_position' => '',
                'el_class'    => ''
            ), $atts ) );

            $el_class = $this->getExtraClass( $el_class );
            $width    = spb_translateColumnWidthToSpan( $width );

            $output .= "\n\t" . '<honestbrew middletext="'.$middle_text.'"></honestbrew>';

            return $output;

        }
    }

    SPBMap::map( 'spb_custom_assets', array(
        "name"   => __( "Honest Vue Circle", 'swift-framework-plugin' ),
        "base"   => "spb_custom_assets",
        "class"  => "spb_custom_assets spb_tab_media",
        "icon"   => "icon-animated-headline",
        "params" => array(
            array(
                "type"        => "textfield",
                "holder"      => "div",
                "heading"     => __( "Middle Text", 'swift-framework-plugin' ),
                "param_name"  => "middle_text",
                "value"       => "",
                "description" => __( "The text that will be display in the middle of the circle", 'swift-framework-plugin' )
            )
        )
    ) );


